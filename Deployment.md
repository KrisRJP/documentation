# Deployment

## Process of deployment

- `git checkout staging`
- `git pull`
- `git checkout production`
- `git pull`
- `git merge staging --no-ff`
- `git push`

After the pipeline is done, start the deploy process via Bitbucket by going to the pipeline and clicking on the deploy button.

Don't forget to put the Jira story statuses on "done".

