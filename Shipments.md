# Shipments



## Shipment flow

![shipment](./resources/shipment.svg)



## Adding a shipment to route planning

To add a new shipment to the route planner, a shipment needs to qualify to three requirements:

- Must have a valid geo-location;
- Shipment needs to have a logical status (i.e. not canceled)
- Delivery date needs to be the current day or in the near future



### Steps to create a shipment:
Make sure that the `QUEUE_DRIVER` property is set on "redis" (instead of "sync") in `.env` file.

#### Adding all shipments
To add all shipments, go to the shipment overview page and click on "all items" filter. Then select all items and click "Add to route planning" button (bottom of the page).

#### Admin app:

- Goto /admin/shipments;
- Click on button "Add" and add a new shipment (fill in company name, address, etc.);
- Edit the date for the delivery to today;
- Start `php artisan horizon`;
- execute `php artisan rjp:plotwise-loop` twice;
- execute `php artisan rjp:update-plotwise-tentative`;
- Goto /admin/route-planning;
- Finalize the route by clicking on the button "Finalize";
- Start `php artisan horizon`;
- Assign a new driver, edit the driver id to 1 and save it.

#### Driver app:

- Goto `/driver`;
- Click on "Dienst Accepteren";
- Edit the "alowed_start_time" column to a date in the past in the "driver_registration" table in the database;
- Refresh the page `/driver` and fill in the vehicle details;
- Edit the "value" column value to the date of today for every single entry in the "hub_settings" table in the database.
  `UPDATE homestead.hub_settings SET value = DATE_FORMAT(NOW(), '%Y-%m-%d') WHERE hub_id > 0;`

#### Troubleshooting
If the shipment is not added to the route planning, check the "admin/manage-plotwise" page to see if the shipment is queued for plotwise.
Check if the shipment is in plotwise on the shipment detail page "Plotwise: In route: {DATE}". If not, click on "Add to planning".

Try to trash buckets from the "admin/manage-plotwise" page and restart the proces of adding shipments to the route planning.


## Resetting a forced shipment

To reset a forced shipment, clear the `driver_app_steps_history ` and `driver_app_steps ` table in the database.