# General information



## Abbreviations

Below is a short list of known abbreviations and corresponding description

| Abbreviation | Description               |
| ------------ | ------------------------- |
| BB           | Bij buren                 |
| BDG          | Bedrijf gesloten          |
| DOP          | Drop-off Point            |
| FTA          | Foutief adres             |
| NA           | Not Available             |
| ND           | Not delivered             |
| NDA          | Leeftijdscontrole mislukt |
| NT           | Niet thuis / Not home     |
| OFD          | Out for delivery          |
| PUL          | Pick-up Location          |
| PUP          | Pick-up Point             |
| SF           | Sorteerfout               |
| TN           | Tijdsnood                 |
| VVB          | Verzenden via Bol.com     |
| VRID         | Vehicle Ride Identifier   |
| WFP          | Waiting for pick-up       |



## Keywords

### Drop-off Point



### Pick-up Location



### Pick-up Point



### Customer



### Sender



### Stopdichtheid



### Hub/Sorteercentrum/Distributiecentrum

