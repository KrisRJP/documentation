# Jira ticket/story: Definition of Ready

This document describes the requirements for a story. A shared understanding by the Product Owner and the Development Team regarding the preferred level of description of Product Backlog items introduced at Sprint Planning.

The purpose of this definition is that we as a team can test each ticket against the following requirements and conditions to see whether a ticket can be executed.



| Requirements                                         | Description                                                  |
| ---------------------------------------------------- | ------------------------------------------------------------ |
| Story is indivisible                                 | No dependencies off other items or team. These dependencies must be investigated and explicitly defined if they are not there. |
| Immediately executable                               | No more questions or uncertainties that still need to be investigated. |
|                    								   |  |
| Value to the customer is described                   | **Als**: Role should be clear |
| 													   | **Wil ik**: Short description which defines one function  |
| 													   | **Zodat**: What is the purpose/goal?  |
| 													   | **Context**: Where does the wish come from? Why is it relevant? Maximum 2 paragraphs.  |
|                    								   |  |
| Acceptance criteria defined                          | **Functionele eisen / Acceptatie criteria**:  Written down point by point. Format: I click on X if Y happens. Or As a short sentence describing one requirement: Only to be used as a client. |
|                           						   | **Testplan / Risico's**: Points for attention for tester. How / what can be tested? Should less obvious places be tested after this change? |
|                           						   | **Notities voor overdracht:** Are there any issues for application management, consultancy or the customer? Does a special mail need to be sent out? Is a conversion script required? Or no special transfer required? Sub-items may need to be created. |
|                    								   |  |
| Agreement between Product Owner and development team | The status “open” indicates the agreement.                   |





\* All **bold text** must be defined in the story/ticket.